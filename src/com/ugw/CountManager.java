package com.ugw;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Manager for the process of counting files in folders and subfolders
 * @author effffgen
 *
 */
public class CountManager {
	private static CountManager instance=null;
	List<Counter> counters;
	/**
	 * Getter for instance of Count Manager
	 * @return instance of this class
	 */
	public static CountManager getInstance() {
		if (instance == null) {
			instance = new CountManager();
		}
		return instance;
	}
	
	/**
	 * Private constructor of CountManager, does nothing at this point
	 */
	private CountManager(){
		counters=new ArrayList<>();
	}

	/**
	 * Starting point for counting process
	 * @param pathList list of all paths that are needed to be scanned
	 */
	public void search(List<String> pathList){
		counters.clear();
		for(int i=0;i<pathList.size();i++){
			counters.add(new Counter(Paths.get(pathList.get(i))));	
			counters.get(i).start();
		}
		for(Counter counter: counters){
			try {
				counter.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Function for emergency stop the counting process (e.g. when user cancels it by pressing ESC).
	 */
	public void stop(){
		for (Counter counter : counters) {
			counter.stopCounting();
		}
	}
	
	
	public List<ResultSet> getResults(){
		List<ResultSet> temp=new ArrayList<>();
		for(Counter c:counters){
			temp.add(c.getResult());
		}
		return temp;
	}
	
}
