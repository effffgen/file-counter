package com.ugw;

public class ResultSet {
	private int count;
	private String path;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	@Override
	public String toString() {
		return path+";"+count;
	}
	public ResultSet(int count, String path) {
		this.count = count;
		this.path = path;
	}
}
