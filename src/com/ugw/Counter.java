package com.ugw;

import java.io.File;
import java.nio.file.Path;

/**
 * 
 * @author effffgen
 */
public class Counter extends Thread {
	/**
	 * 
	 */
	private volatile boolean stop = false;
	private Path targetPath;
	private int count;
	private ResultSet results;

	public Counter(Path path) {
		count = 0;
		targetPath = path;
		results=null;
	}

	@Override
	public void run() {
		if (targetPath.toFile().exists()) {
			search(targetPath.toFile());
		}
		results = new ResultSet(count, targetPath.toString());
	}

	public ResultSet getResult() {
		return results;
	}

	public void search(File file) {
		File[] filesInDirectory = file.listFiles();
		//In the case program can't gain access to directory and list of files is null
		if(filesInDirectory!=null&&!stop)
		for (int j = 0; j < filesInDirectory.length; j++) {
			if (filesInDirectory[j].isDirectory()){
				search(filesInDirectory[j]);	
			}
			else{
				count++;
			}
		}	
	}

	public void stopCounting() {
		stop=true;
	}
}
