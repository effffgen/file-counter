package com.ugw;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyAdapter;
import org.jnativehook.keyboard.NativeKeyEvent;


/**
 * @author effffgen
 *
 */
public class FileCounterApp {

	public static void main(String args[]) throws NativeHookException {
		if (args.length != 2) {
			// Wrong arguments
			System.out.println("Usage:\n" + "file-counter.exe file1 file2\n" + "file1 - input file with paths,\n"
					+ "file2 - location of file with results");
			System.exit(0);
		}
		new FileCounterApp(args[0], args[1]).start();
	}

	private String inputFile;
	private String outputFile;
	private CountManager manager;

	public FileCounterApp(String file1, String file2) {
		this.inputFile = file1;
		this.outputFile = file2;
		manager = CountManager.getInstance();
	}

	public void start() throws NativeHookException {
		// Creating means to stop the app
		initHook();

		// open input file and get all strings

		List<String> paths = getPaths();

		// Starting search
		manager.search(paths);

		// showing results and writing them to file
		print();

		// app is closing, stop waiting for pressing esc
		GlobalScreen.unregisterNativeHook();

	}

	private List<String> getPaths() {
		Path inputFile = Paths.get(this.inputFile);
		List<String> paths = null;
		try {
			paths = Files.readAllLines(inputFile);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return paths;
	}

	private void initHook() throws NativeHookException {
		// disable System.out and turn off logging
		PrintStream origOut = System.out;
		System.setOut(null);
		// register the native hook and key listener
		GlobalScreen.registerNativeHook();

		GlobalScreen.addNativeKeyListener(new NativeKeyAdapter() {
			@Override
			public void nativeKeyPressed(NativeKeyEvent e) {
				// waiting for esc
				if (e.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
					//sending stop signal
					manager.stop();
				}
			}
		});
		// restore System.out
		System.setOut(origOut);
	}

	private void print() {
		List<ResultSet> result = manager.getResults();
		//a little bit of magic
		System.out.format("%-3s\t%-50s\t%-3s\n", "�", "Path", "Count");

		int i=1;
		// opening the output file
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputFile), Charset.forName("UTF-8"))) {
			for (ResultSet set : result) {
				//
				writer.write(set.toString()+"\n");
				System.out.format("%-3d\t%-50s\t%-3d\n", i++, set.getPath(), set.getCount());
			}
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}

	}
}
